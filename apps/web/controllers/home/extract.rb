module Web::Controllers::Home
  class Extract
    include Web::Action
    expose :upcs

    def call(params)
      @upcs = JSON.parse(params[:extract][:response])['tdatas'][1..-1].map { |line| line[0].to_i }
    rescue
      @upcs = []
    end
  end
end
