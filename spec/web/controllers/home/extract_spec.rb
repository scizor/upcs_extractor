require 'spec_helper'
require_relative '../../../../apps/web/controllers/home/extract'

describe Web::Controllers::Home::Extract do
  let(:action) { Web::Controllers::Home::Extract.new }
  let(:params) { Hash[] }

  it 'is successful' do
    response = action.call(params)
    response[0].must_equal 200
  end
end
